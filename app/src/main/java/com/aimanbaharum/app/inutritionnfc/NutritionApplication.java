package com.aimanbaharum.app.inutritionnfc;

import android.app.Application;

import io.realm.Realm;

/**
 * Created by aimanb on 10/4/2018.
 */

public class NutritionApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Realm.init(this);
        Realm realm = Realm.getDefaultInstance();
    }
}
